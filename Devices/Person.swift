//
//  Person.swift
//  Devices
//
//  Created by Tyler Carlile on 12/1/20.
//

import Foundation
import CoreData

class Person: NSManagedObject, Identifiable {
    @NSManaged var name: String
    @NSManaged var devices: NSSet?
}
