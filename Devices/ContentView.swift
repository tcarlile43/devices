//
//  ContentView.swift
//  Devices
//
//  Created by Tyler Carlile on 12/1/20.
//

import SwiftUI
import CoreData

struct ContentView: View {
    @Environment(\.managedObjectContext) private var viewContext

    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \Person.name, ascending: true)],
        animation: .default)
    private var people: FetchedResults<Person>
    @State private var name: String = ""

    var body: some View {
        NavigationView {
            List {
                Section(header: Text("New Person")) {
                    TextField("Name", text: $name)
                    Button(action: {
                        do {
                            let person = Person(context: viewContext)
                            person.name = name
                            try viewContext.save()
                        } catch {
                            let nsError = error as NSError
                            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
                        }
                    }) {
                        Text("Add").foregroundColor(.blue)
                    }
                }
                Section (header: Text("People")) {
                    ForEach(people) { person in
                        NavigationLink(destination: PersonView(person: person)) {
                            Text("\(person.name)")
                        }
                    }
                    .onDelete(perform: deleteItems)
                }
            }
        }
    }

    private func deleteItems(offsets: IndexSet) {
        withAnimation {
            offsets.map { people[$0] }.forEach(viewContext.delete)

            do {
                try viewContext.save()
            } catch {
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }
}

struct PersonView: View {
    @Environment(\.managedObjectContext) private var viewContext
    
    @ObservedObject private var person: Person
    @FetchRequest private var devices: FetchedResults<Device>
    @State private var name: String = ""
    
    init(person: Person) {
        self.person = person
        self._devices = FetchRequest(entity: Device.entity(),
                                     sortDescriptors: [NSSortDescriptor(keyPath: \Device.name, ascending: true)],
                                     predicate: NSPredicate(format: "owner = %@", person))
    }
    
    var body: some View {
        List {
            Section(header: Text("New Device")) {
                TextField("Name", text: $name)
                Button(action: {
                    do {
                        let device = Device(context: viewContext)
                        device.name = name
                        device.owner = person
                        try viewContext.save()
                    } catch {
                        let nsError = error as NSError
                        fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
                    }
                }) {
                    Text("Add").foregroundColor(.blue)
                }
            }
            Section(header: Text("Devices")) {
                ForEach(devices) { device in
                    Text("\(device.name)")
                }
                .onDelete(perform: deleteItems)
            }
        }
    }
    
    private func deleteItems(offsets: IndexSet) {
        withAnimation {
            offsets.map { devices[$0] }.forEach(viewContext.delete)

            do {
                try viewContext.save()
            } catch {
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }
}
