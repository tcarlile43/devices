//
//  DevicesApp.swift
//  Devices
//
//  Created by Tyler Carlile on 12/1/20.
//

import SwiftUI

@main
struct DevicesApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
